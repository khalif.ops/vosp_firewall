﻿using NetFwTypeLib;
using System;
using System.Windows.Forms;

namespace VoSP_Firewall
{
    public partial class frmMain : Form
    {
        int typeFW;
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnBrowseGTAV_Click(object sender, EventArgs e)
        {
            openGTAV.ShowDialog();
            txtGameDir.Text = openGTAV.FileName;
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            if (typeFW > 0) tickEXEpath.Checked = false;
            vospFirewall(typeFW, tickEXEpath.Checked);
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            toggleAddFW.Checked = true;
        }
        private void toggleAddFW_CheckedChanged(object sender, EventArgs e)
        {
            if (toggleAddFW.Checked == true && toggleRemFW.Checked == false) { typeFW = 0; }
            else if (toggleAddFW.Checked == false && toggleRemFW.Checked == true) { typeFW = 1; }

        }

        private void toggleRemFW_CheckedChanged(object sender, EventArgs e)
        {
            if (toggleRemFW.Checked == true && toggleAddFW.Checked == false) { typeFW = 1; }
            else if (toggleRemFW.Checked == false && toggleAddFW.Checked == true) { typeFW = 0; };
        }



        private void tickEXEpath_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void vospFirewall(int typeFW, bool useExe)
        {
            //type fw= 0 to add, 1 to delete, 2 to check if it's there, 3 to deactivate.
            //GTA V online firewall configs
            //make a rule
            bool isFW = false;
            INetFwRule firewallRule = (INetFwRule)Activator.CreateInstance(
                Type.GetTypeFromProgID("HNetCfg.FWRule"));

            //applicator
            INetFwPolicy2 firewallPolicy = (INetFwPolicy2)Activator.CreateInstance(
                Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));

            if (useExe == true) firewallRule.ApplicationName = txtGameDir.Text;//optional, get it from TXT this is working with spaced directories.
            firewallRule.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_OUT;//outward
            firewallRule.Action = NET_FW_ACTION_.NET_FW_ACTION_BLOCK;//blocking
            firewallRule.Protocol = (int)NET_FW_IP_PROTOCOL_.NET_FW_IP_PROTOCOL_UDP;//udp. The Protocol property must be set before the LocalPorts or RemotePorts properties or an error will be returned.
            firewallRule.LocalPorts = "6672,61455-61458";
            firewallRule.Description = "lorem ipsum";
            firewallRule.Enabled = true;
            firewallRule.InterfaceTypes = "All";//wifi, ethernet,
            firewallRule.Name = $"GTA5_SOLOPUB";

            if (typeFW == 0)
            {
                try
                { var rule = firewallPolicy.Rules.Item(firewallRule.Name); }//we relied on System.IO.FileNotFoundException that thrown because it's not found....
                catch (Exception e)
                {// in order to make rule!
                    firewallPolicy.Rules.Add(firewallRule);//make gta5_solopub
                    MessageBox.Show("Firewall rule added! ", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    isFW = true;//do not pop up firewall already existed
                }
                if (useExe == true)
                {
                    var rule = firewallPolicy.Rules.Item(firewallRule.Name);
                    rule.ApplicationName = txtGameDir.Text;//update gamedir.
                    MessageBox.Show("Firewall rule updated to include game path!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (isFW == false) MessageBox.Show("Firewall rule already exists!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);//stop adding firewall rules
                }
            }
            else if (typeFW == 1)
            {
                firewallPolicy.Rules.Remove(firewallRule.Name);//delete rule of gta5_solopub
                MessageBox.Show("Firewall rule is deleted!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //verified to working!!!
            }
            else if (typeFW == 2)
            {
                try
                { var rule = firewallPolicy.Rules.Item(firewallRule.Name); }//we relied on System.IO.FileNotFoundException that thrown because it's not found....
                catch (Exception e)
                {// in order to make rule!
                    toggleAddFW.Checked = true;
                }
            }
        }

        private void lbDebug_Click(object sender, EventArgs e)
        {
            //lbDebug.Text = Convert.ToString(typeFW)+ tickEXEpath.Checked;
        }

        private void lnkAbout_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MessageBox.Show("VoSP Firewall\nMade by bitelaserkhalif\nCopyright © bitelaserkhalif 2020\nLicensed under BSD license", "About", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
