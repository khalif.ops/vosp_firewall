﻿namespace VoSP_Firewall
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.openGTAV = new System.Windows.Forms.OpenFileDialog();
            this.toggleAddFW = new System.Windows.Forms.RadioButton();
            this.toggleRemFW = new System.Windows.Forms.RadioButton();
            this.grpFWconf = new System.Windows.Forms.GroupBox();
            this.txtGameDir = new System.Windows.Forms.TextBox();
            this.btnBrowseGTAV = new System.Windows.Forms.Button();
            this.tickEXEpath = new System.Windows.Forms.CheckBox();
            this.btnApply = new System.Windows.Forms.Button();
            this.lbDebug = new System.Windows.Forms.Label();
            this.lnkAbout = new System.Windows.Forms.LinkLabel();
            this.grpFWconf.SuspendLayout();
            this.SuspendLayout();
            // 
            // openGTAV
            // 
            this.openGTAV.DefaultExt = "exe";
            this.openGTAV.FileName = "GTA5.exe";
            this.openGTAV.Filter = "GTA5.exe|GTA5.exe";
            this.openGTAV.Title = "Browse GTA5.exe...";
            // 
            // toggleAddFW
            // 
            this.toggleAddFW.AutoSize = true;
            this.toggleAddFW.Location = new System.Drawing.Point(6, 25);
            this.toggleAddFW.Name = "toggleAddFW";
            this.toggleAddFW.Size = new System.Drawing.Size(82, 17);
            this.toggleAddFW.TabIndex = 0;
            this.toggleAddFW.TabStop = true;
            this.toggleAddFW.Text = "Add Firewall";
            this.toggleAddFW.UseVisualStyleBackColor = true;
            this.toggleAddFW.CheckedChanged += new System.EventHandler(this.toggleAddFW_CheckedChanged);
            // 
            // toggleRemFW
            // 
            this.toggleRemFW.AutoSize = true;
            this.toggleRemFW.Location = new System.Drawing.Point(6, 48);
            this.toggleRemFW.Name = "toggleRemFW";
            this.toggleRemFW.Size = new System.Drawing.Size(103, 17);
            this.toggleRemFW.TabIndex = 0;
            this.toggleRemFW.TabStop = true;
            this.toggleRemFW.Text = "Remove Firewall";
            this.toggleRemFW.UseVisualStyleBackColor = true;
            this.toggleRemFW.CheckedChanged += new System.EventHandler(this.toggleRemFW_CheckedChanged);
            // 
            // grpFWconf
            // 
            this.grpFWconf.Controls.Add(this.txtGameDir);
            this.grpFWconf.Controls.Add(this.btnBrowseGTAV);
            this.grpFWconf.Controls.Add(this.tickEXEpath);
            this.grpFWconf.Controls.Add(this.toggleAddFW);
            this.grpFWconf.Controls.Add(this.toggleRemFW);
            this.grpFWconf.Location = new System.Drawing.Point(12, 12);
            this.grpFWconf.Name = "grpFWconf";
            this.grpFWconf.Size = new System.Drawing.Size(363, 74);
            this.grpFWconf.TabIndex = 1;
            this.grpFWconf.TabStop = false;
            this.grpFWconf.Text = "Firewall Config";
            // 
            // txtGameDir
            // 
            this.txtGameDir.Location = new System.Drawing.Point(257, 19);
            this.txtGameDir.Name = "txtGameDir";
            this.txtGameDir.ReadOnly = true;
            this.txtGameDir.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.txtGameDir.Size = new System.Drawing.Size(100, 20);
            this.txtGameDir.TabIndex = 3;
            // 
            // btnBrowseGTAV
            // 
            this.btnBrowseGTAV.Location = new System.Drawing.Point(257, 45);
            this.btnBrowseGTAV.Name = "btnBrowseGTAV";
            this.btnBrowseGTAV.Size = new System.Drawing.Size(100, 23);
            this.btnBrowseGTAV.TabIndex = 2;
            this.btnBrowseGTAV.Text = "browse GTA5.exe";
            this.btnBrowseGTAV.UseVisualStyleBackColor = true;
            this.btnBrowseGTAV.Click += new System.EventHandler(this.btnBrowseGTAV_Click);
            // 
            // tickEXEpath
            // 
            this.tickEXEpath.AutoSize = true;
            this.tickEXEpath.Location = new System.Drawing.Point(158, 19);
            this.tickEXEpath.Name = "tickEXEpath";
            this.tickEXEpath.Size = new System.Drawing.Size(93, 17);
            this.tickEXEpath.TabIndex = 1;
            this.tickEXEpath.Text = "with EXE path";
            this.tickEXEpath.UseVisualStyleBackColor = true;
            this.tickEXEpath.CheckedChanged += new System.EventHandler(this.tickEXEpath_CheckedChanged);
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(18, 102);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(93, 32);
            this.btnApply.TabIndex = 3;
            this.btnApply.Text = "Apply Changes";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // lbDebug
            // 
            this.lbDebug.AutoSize = true;
            this.lbDebug.Enabled = false;
            this.lbDebug.Location = new System.Drawing.Point(281, 112);
            this.lbDebug.Name = "lbDebug";
            this.lbDebug.Size = new System.Drawing.Size(57, 13);
            this.lbDebug.TabIndex = 4;
            this.lbDebug.Text = "debugging";
            this.lbDebug.Visible = false;
            this.lbDebug.Click += new System.EventHandler(this.lbDebug_Click);
            // 
            // lnkAbout
            // 
            this.lnkAbout.AutoSize = true;
            this.lnkAbout.Location = new System.Drawing.Point(153, 112);
            this.lnkAbout.Name = "lnkAbout";
            this.lnkAbout.Size = new System.Drawing.Size(35, 13);
            this.lnkAbout.TabIndex = 5;
            this.lnkAbout.TabStop = true;
            this.lnkAbout.Text = "About";
            this.lnkAbout.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkAbout_LinkClicked);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 149);
            this.Controls.Add(this.lnkAbout);
            this.Controls.Add(this.lbDebug);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.grpFWconf);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "VoSP Firewall";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.grpFWconf.ResumeLayout(false);
            this.grpFWconf.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openGTAV;
        private System.Windows.Forms.RadioButton toggleAddFW;
        private System.Windows.Forms.RadioButton toggleRemFW;
        private System.Windows.Forms.GroupBox grpFWconf;
        private System.Windows.Forms.Button btnBrowseGTAV;
        private System.Windows.Forms.CheckBox tickEXEpath;
        private System.Windows.Forms.TextBox txtGameDir;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Label lbDebug;
        private System.Windows.Forms.LinkLabel lnkAbout;
    }
}

